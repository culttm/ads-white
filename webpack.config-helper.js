'use strict';

const Path = require('path')
const Webpack = require('webpack')
const HtmlWebpackPlugin = require('html-webpack-plugin')
const ExtractTextPlugin = require('extract-text-webpack-plugin');
const ExtractSASS = new ExtractTextPlugin('styles/bundle.css');
const Autoprefixer = require('autoprefixer')
const SpritesmithPlugin = require('webpack-spritesmith')

module.exports = (options) => {

    let webpackConfig = {
        devtool: options.devtool,
        entry: [
            `webpack-dev-server/client?http://localhost:${options.port}`,
            'webpack/hot/dev-server',
            './src/scripts/index'
        ],
        output: {
            path: Path.join(__dirname, 'dist'),
            filename: 'js/bundle.js',
            publicPath: ''
        },
        resolve: {
            modulesDirectories: ["bower_components", "node_modules", "sprite-generated"]
        },
        plugins: [
            new Webpack.ProvidePlugin({
                $: "jquery",
                jQuery: "jquery",
                "window.jQuery": "jquery"
            }),
            new Webpack.DefinePlugin({
                'process.env': {
                    NODE_ENV: JSON.stringify(options.isProduction ? 'production' : 'development')
                }
            }),
            new HtmlWebpackPlugin({
                template: './src/index.html',
                minify: false,
                cache: false,
            }),
            new SpritesmithPlugin({
                src: {
                    cwd: Path.resolve(__dirname, 'src/images/sprite'),
                    glob: '*.png'
                },
                target: {
                    image: Path.resolve(__dirname, 'src/sprite-generated/sprite.png'),
                    css: Path.resolve(__dirname, 'src/sprite-generated/_sprite.scss')
                },
                apiOptions: {
                    cssImageRef: "~sprite.png"
                }
            })
        ],
        module: {
            loaders: [
                {
                    test: /\.js$/,
                    exclude: /(node_modules|bower_components)/,
                    loader: 'babel',
                    query: {
                        presets: ['es2015']
                    }
                },
                {
                    test: /\.(png|jpg|gif)$/,
                    loader: 'file-loader?name=/images/[name].[ext]'
                },
                {
                    test: /\.(eot|svg|ttf|woff|woff2)$/,
                    loader: 'file-loader?name=/fonts/[name].[ext]'
                },
                {
                    test: /\.html$/,
                    loader: "html-loader?minimize=false"
                }
            ]
        },
        postcss: function () {
            return [
                Autoprefixer,
                require('postcss-pxtorem')({
                    replace: false,
                    selectorBlackList: ['html'],
                    mediaQuery: false
                }),
                require('postcss-flexibility')(),
                require('css-mqpacker')()
            ];
        }
    };

    if (options.isProduction) {
        webpackConfig.entry = ['./src/scripts/index'];

        // webpackConfig.output.publicPath = '../';

        webpackConfig.plugins.push(
            new Webpack.optimize.OccurenceOrderPlugin(),
            // new Webpack.optimize.UglifyJsPlugin({
            //     compressor: {
            //         warnings: false
            //     }
            // }),
            new HtmlWebpackPlugin({
                template: './src/index.html',
                minify: false,
                cache: false
            }),
            ExtractSASS
        );

        webpackConfig.module.loaders.push({
            test: /\.scss$/i,
            loader: ExtractSASS.extract(['css', 'resolve-url', 'postcss-loader', 'sass'])
        });

        webpackConfig.postcss = function () {
            return[
                Autoprefixer,
                require('postcss-pxtorem')({
                    replace: false,
                    selectorBlackList: ['html'],
                    mediaQuery: false
                }),
                require('postcss-flexibility')(),
                require('css-mqpacker')()
            ]
        }


    } else {
        webpackConfig.plugins.push(
            new Webpack.HotModuleReplacementPlugin()
        );

        webpackConfig.module.loaders.push(
            {
                test: /\.scss$/i,
                loader: "style!css!postcss-loader!sass"
            },
            {
                test: /\.js$/,
                loader: 'eslint',
                exclude: /node_modules/
            }
        );

        webpackConfig.devServer = {
            contentBase: './dist',
            hot: true,
            port: options.port,
            inline: true,
            progress: true,
            outputPath: Path.join(__dirname, 'dist')
        };
    }

    return webpackConfig;

}