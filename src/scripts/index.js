'use strict';
import './required';
import classie from 'desandro-classie';
const flexibility = require('flexibility');

//============================================
for(var i = 0; i < document.getElementsByClassName('js-nav-trigger').length; i++){
    document.getElementsByClassName('js-nav-trigger')[i].addEventListener('click', () => {
        classie.toggle( document.getElementsByTagName('body')[0], 'nav-active' );
    }, false);
}

flexibility( document.documentElement );